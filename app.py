from flask import Flask, render_template, request, url_for, session, redirect
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.table import Table
import io
import base64
import numpy as np
import obrazek
import random

#import of our modules
import flintrandmurd
import flintmurd

import flintdips
import flintranddips

import flinttheft
import flintrandtheft



app=Flask(__name__)
app.secret_key = b'$IkTp#13#stoKrm@'

#random choosing of MVP ;)));
yourmomgay = random.randint(1,30) #yeah she is;
#print(yourmomgay) to check what do we have;
print(yourmomgay)

class rager:
    def __init__(self, zlosc, spokoj, ktore, calka):
        self.zlosc=zlosc
        self.spokoj=spokoj
        self.ktore=ktore
        self.calka=calka
        
@app.route("/")
def glowna():

    session['creep1'] = 0
    session['creep2'] = 0
    session['creep3'] = 0

    img = io.BytesIO()
    fig,ax = plt.subplots()
    ax.set_axis_off()
    tabela=obrazek.tabelka(ax)
    ax.add_table(tabela)
    plt.savefig(img, format='png')
    img.seek(0)
    url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    rysunek='data:image/png;base64,{}'.format(url)
    return render_template("index.html", rysunek=rysunek)

@app.route("/o_nas")
def to_my():
    return render_template("zdjecia.html")

@app.route("/gra", methods=["POST", "GET"])
def gra():
    creep1 = int(session.get('creep1'))
    creep2 = int(session.get('creep2'))
    creep3 = int(session.get('creep3'))
    

    
    if request.method=="POST":
        if "rage" in session:
            s=session["rage"]
            z=s[0]["zlosc"]
            c=s[0]["spokoj"]
            ktory=s[0]["ktore"]
            licznik=s[0]["ktore"]
            calka=s[0]["calka"]
            odpowiedz=request.form["odpowiedz"]
            
            #################################################################################################
            #added 2 other options: theft and dipsi;
            if yourmomgay <= 10 - creep1:
                odpowiedz, x, y, narracja,czykoniec,calka = flintmurd.akcja(odpowiedz, licznik,z,c,calka)
                if creep1 <= 8:
                    session['creep1'] = creep1 + 2
            if 10 - creep1< yourmomgay <= 20 - creep2:
                odpowiedz, x, y, narracja,czykoniec,calka = flintdips.akcja(odpowiedz, licznik,z,c,calka)
                if creep2 <= 8:
                    session['creep2'] = creep2 + 1
            if 20 - creep2 < yourmomgay <= 30:
                odpowiedz, x, y, narracja,czykoniec,calka = flinttheft.akcja(odpowiedz, licznik,z,c,calka)
                if creep3 <= 8:
                    session['creep3'] = creep3 + 2
            #################################################################################################
            if czykoniec==0 or czykoniec==5:
                Rager=rager(z+x,c+y,ktory+1, calka)
                a=Rager.zlosc
                k=Rager.spokoj
                session["rage"]=[Rager.__dict__]
            elif czykoniec==1:
                return redirect(url_for('przegrana1'))
            elif czykoniec==2:
                return redirect(url_for('przegrana2'))
            elif czykoniec==3:
                return redirect(url_for('wygrana2'))
            elif czykoniec==4:
                return redirect(url_for('wygrana1'))
            print(creep1,creep2,creep3)

    if request.method=="GET" and yourmomgay <= 10 - creep1:
        odpowiedz="Witaj przechodniu. Co Cię sprowadza w tak piękny wieczór? Chcesz może skorzystać z brzytwy? Garoty? Kulki?"
        Rager=rager(0, 0, 0, 0)
        a=0
        k=0
        narracja="Wędrujesz nocą po Wrocławiu. Skręcasz w ulicę Traugutta. Nagle zaczepia cię tajemnicza postać."
        session["rage"]=[Rager.__dict__]    
    if request.method=="GET" and 20 < yourmomgay <= 30 - creep3:
        odpowiedz="Heh no i co? Masz może ze sobą coś cennego? Podzielimy się tym!"
        Rager=rager(0, 0, 0, 0)
        a=0
        k=0
        narracja="Wędrujesz nocą po Wrocławiu. Skręcasz w ulicę Traugutta. Nagle zaczepia cię tajemnicza postać."
        session["rage"]=[Rager.__dict__] 
    if request.method=="GET" and 10 < yourmomgay <= 20 - creep2:
        odpowiedz="Pan piękny jak powróże! Hugo Bossa mam dla Pana pięknego za 20 złotych! Piękne perfuma chce kupić?"
        Rager=rager(0, 0, 0, 0)
        a=0
        k=0
        narracja="Wędrujesz nocą po Wrocławiu. Skręcasz w ulicę Traugutta. Nagle zaczepia cię tajemnicza postać."
        session["rage"]=[Rager.__dict__] 
#####################################################
#3 different options of html or one html
#####################################################
    # if yourmomgay == 1:
    #     return render_template("gra.html", odpowiedz=odpowiedz, a=a, k=k, narracja=narracja)
    # if yourmomgay == 2:
    #     return render_template("gra.html", odpowiedz=odpowiedz, a=a, k=k, narracja=narracja)
    # if yourmomgay == 3:
    #     return render_template("gra.html", odpowiedz=odpowiedz, a=a, k=k, narracja=narracja)
    return render_template("gra.html", odpowiedz=odpowiedz, a=a, k=k, narracja=narracja)
    

@app.route("/instrukcja")
def instrukcja():
    return render_template("instrukcja.html")

@app.route("/kasia")
def kasia():
    return render_template("kasia.html")

@app.route("/bodzia")
def bodzia():
    return render_template("bodzia.html")

@app.route("/andrzej")
def andrzej():
    return render_template("andrzej.html")

@app.route("/wygrana1")
def wygrana1():
    return render_template("wygrana1.html")

@app.route("/wygrana2")
def wygrana2():
    return render_template("wygrana2.html")

@app.route("/przegrana1")
def przegrana1():
    return render_template("przegrana1.html")

@app.route("/przegrana2")
def przegrana2():
    return render_template("przegrana2.html")
def akcja(odpowiedz, licznik, rage, calm, calka):
	import flintranddips
	import random
	odpowiedz=odpowiedz.lower()
	slowa = odpowiedz.split(' ')

	#the 'answer' is a list of lines from txt file
	answers = open('newdipsans.txt','r')
	answer = answers.readlines()

	#definicja odpowiedzi
	odp5 = answer[0]
	odp51 = answer[1]
	odp4 = answer[2]
	odp41 = answer[3]
	odp3 = answer[4]
	odp31=answer[5]
	odp2 = answer[6]
	odp21 = answer[7]
	odp1 = answer[8]
	odp11 = answer[9]

	if len(odpowiedz)>=1:
		if rage<=10 and calm<=10:
			czykoniec=0
			if licznik==0:
				calka=0
				r=random.randint(1,50)
				if r==1:
					narracja=answer[10]
				elif r==2:
					narracja=answer[11]
				elif r==3:
					narracja=answer[12]
				elif r==20:
					narracja=answer[13]
					x = 2
				elif r==30:
					narracja=answer[14]
				elif r==40:
					narracja=answer[15]
				elif r==45:
					narracja=answer[16]
					y = 1
				elif r==50:
					narracja=answer[17]
				else:
					narracja=answer[18]

				if 'tak' in odpowiedz:
					odpowiedz = answer[19]
					x=2
					y=0

				elif 'dziękuję' in odpowiedz:
					odpowiedz = answer[20]
					x=5
					y=0
			
				elif 'spokojnie' in odpowiedz:
					odpowiedz = answer[21]
					x=3
					y=0

				elif 'spadaj' in odpowiedz:
					odpowiedz = answer[22]
					x=80
					y=0

				elif 'poczekaj' in odpowiedz:
					odpowiedz = answer[23]
					x=0
					y=1
				
				elif 'mój bohaterze' in odpowiedz:
					odpowiedz = answer[24]
					x=0
					y=1
				
				elif 'ty' in slowa:
					odpowiedz = answer[25]
					x=1
					y=0

				elif 'mam' and ('nóż' or 'pistolet' or 'broń' or 'kose' or 'gaz' or 'karabin' or 'ojca prokuratora') in odpowiedz:
					odpowiedz = answer[26]
					x=1
					y=0
				

				elif 'w jedynce palą od września' in odpowiedz:
					odpowiedz = answer[27]
					x=0
					y=20

				elif 'kto cię wysłał' in odpowiedz:
					odpowiedz = answer[28]
					x=5
					y=0
				
				elif 'kim jest najwyższy?' in odpowiedz:
					odpowiedz = answer[29]
					x=20
					y=0
				
				elif 'ciebie' in odpowiedz:
					odpowiedz = answer[30]
					x=10
					y=0

				elif 'twoje' in odpowiedz:
					odpowiedz = answer[31]
					x=20
					y=0

				elif 'czego chcesz' in odpowiedz:
					odpowiedz = answer[32]
					x=10
					y=0

				elif 'policja' in odpowiedz:
					odpowiedz = answer[33]
					x=5
					y=0

				elif 'dzieci' in odpowiedz:
					odpowiedz = answer[34]
					x=10
					y=0

				elif 'nie' and ('dziękuję' or 'dziekuje') in slowa:
					odpowiedz = answer[35]
					x=10
					y=0

				elif 'nie' in slowa:
					odpowiedz = answer[36]
					x=5
					y=0
				
				elif 'ratunku' in odpowiedz:
					odpowiedz = answer[37]
					x=4
					y=0

				elif 'pomocy' in odpowiedz:
					odpowiedz = answer[38]
					x=2
					y=0
	
				else:
					odpowiedz = answer[39]
					x=0
					y=0.5
			else:
				r=random.randint(1,50)
				if r==1:
					narracja=answer[40]
				elif r==2:
					narracja=answer[41]
				elif r==3:
					narracja=answer[42]
				
				else:
					narracja=answer[43] 
				if licznik==5:
					odpowiedz = answer[44]
					x=0
					y=1
					calka=0

				elif licznik==6:
					calka=0
					if 'andrzej' in odpowiedz:
						odpowiedz = answer[45]
						x=0
						y=10


					elif 'kasia' in odpowiedz:
						odpowiedz = answer[46]
						x=0
						y=10

					elif 'bodzia' in odpowiedz:
						odpowiedz = answer[47]
						x=0
						y=10

					elif 'mateusz' in odpowiedz:
						odpowiedz = answer[48]
						x=0
						y=10
			
					elif 'nie powiem' in odpowiedz:
						odpowiedz=answer[49]
						x=7
						y=0

					elif 'spadaj' in odpowiedz:
						odpowiedz=answer[50]
						x=10
						y=0

					else:
						odpowiedz=answer[51]
						x=0
						y=1
	
				elif licznik == 9:
					narracja = answer[52]
					x=0
					y=0.5

					if r<10:
						odpowiedz=answer[53]
						calka = 1
					if r>=10 and r<20:
						odpowiedz=answer[54]
						calka = 2
					if r>=20 and r<30:
						odpowiedz=answer[55]
						calka = 3
					if r>=30 and r<40:
						odpowiedz=answer[56]
						calka = 4
					if r>=40 and r<=50:
						odpowiedz=answer[57]
						calka = 5
	
				elif licznik == 10:
					calka=calka
					if calka == 1 and odpowiedz == odp1:
						odpowiedz = answer[58]
						x = 0
						y = 40

					if calka == 1 and odpowiedz == odp11:
						odpowiedz = answer[59]
						x = 0
						y = 40

					elif calka == 2 and odpowiedz == odp2:
						odpowiedz = answer[60]
						x = 0
						y = 40

					elif calka == 2 and odpowiedz == odp21:
						odpowiedz=answer[61]
						x=0
						y=40

					elif calka == 3 and odpowiedz == odp3:
						odpowiedz = answer[62]
						x = 0
						y = 40

					elif calka == 3 and odpowiedz == odp31:
						odpowiedz = answer[63]
						x = 0
						y = 40

					elif calka == 4 and odpowiedz == odp4:
						odpowiedz = answer[64]
						x = 0
						y = 40

					elif calka == 4 and odpowiedz == odp41:
						odpowiedz=answer[65]
						x=0
						y=40

					elif calka == 5 and odpowiedz == odp5:
						odpowiedz = answer[66]
						x = 0
						y = 40

					elif calka==5 and odpowiedz == odp51:
						odpowiedz=answer[67]
						x=0
						y=40

					else:
						odpowiedz = answer[68]
						x = 20
						y = 0
						
				else:
					if 'czemu' in odpowiedz:
						odpowiedz=answer[69]
						x=1
						y=0

					elif 'błagam' in odpowiedz:
						odpowiedz=answer[70]
						x=0
						y=5
			
					elif 'oszczędź' in odpowiedz:
						odpowiedz=answer[71]
						x=0
						y=3

					elif 'ty' in odpowiedz:
						odpowiedz=answer[72]
						x=1
						y=0
				
					elif 'ciebie' in odpowiedz:
						odpowiedz=answer[73]
						x=1.5
						y=0

					elif 'dlaczego' in odpowiedz:
						odpowiedz=answer[74]
						x=1
						y=0
					elif "policja" in odpowiedz:
						odpowiedz=answer[75]
						x=1
						y=0
					elif 'tak' in odpowiedz:
						odpowiedz=answer[76]
						x=0
						y=1
					elif 'nie' in odpowiedz:
						odpowiedz=answer[77]
						x=1
						y=0
					elif 'ratunku' in odpowiedz:
						odpowiedz=answer[78]
						x=1
						y=0
					elif 'pomocy' in odpowiedz:
						odpowiedz=answer[79]
						x=1
						y=0
					elif 'kto to' in odpowiedz:
						odpowiedz=answer[80]
						x=1
						y=0
					elif 'matematyka' in odpowiedz:
						odpowiedz = answer[81]
						x=0
						y=1
					elif 'matematyki' in odpowiedz:
						odpowiedz = answer[82]
						narracja = answer[83]
						x=10
						y=0
					elif 'nic' in odpowiedz:
						odpowiedz=answer[84]
						x=0
						y=0
					
					elif 'wszystko' in odpowiedz:
						odpowiedz=answer[85]
						x=1
						y=0
					
					elif 'zycie' in odpowiedz:
						odpowiedz=answer[86]
						x=0
						y=1

					elif 'rodzina' in odpowiedz:
						odpowiedz=answer[87]
						x=0
						y=0
					
					elif 'leszno' in odpowiedz:
						odpowiedz=answer[88]
						x=0
						y=1

					elif 'kłodzko' in odpowiedz:
						odpowiedz=answer[89]
						x=0
						y=2
					
					elif 'git' in odpowiedz:
						odpowiedz=answer[90]
						x=0
						y=0

					elif 'flask' in odpowiedz:
						odpowiedz=answer[91]
						x=8
						y=0

					elif 'przepraszam' in odpowiedz:
						odpowiedz=answer[92]
						x=0
						y=2

					else:
						odpowiedz=flintranddips.flintrand()
						x=0
						y=0.5

		elif rage>10 and calm<5:
			czykoniec=1
			odpowiedz=answer[93]
			x=0
			y=0
			narracja = answer[94]
			calka=0
	
		elif rage>10 and calm>=5:
			czykoniec=2
			odpowiedz= answer[95]
			x=0
			y=0
			narracja = answer[96]
			calka=0

		elif rage<5 and calm>10:
			czykoniec=3
			odpowiedz= answer[97]
			x=0
			y=0
			narracja= answer[98]
			calka=0

		elif rage>=5 and calm>10:
			czykoniec=4
			odpowiedz= answer[99]
			x=0
			y=0
			narracja= answer[100]
			calka=0
		else:
			calka=0
			if rage<=10 and calm<=10:
				czykoniec=5
				if licznik%2 == 0:
					odpowiedz=answer[101]
					narracja = answer[102]
				if licznik%3 == 0:
					odpowiedz = answer[103]
				else:
					odpowiedz=answer[104]
					narracja =answer[105]
				x=3
				y=0
				narracja = answer[106]
	
			elif rage>10 and calm<5:
				czykoniec=1
				odpowiedz=answer[107]
				x=0
				y=0
				narracja = answer[108]
				
			elif rage>10 and calm>=5:
				czykoniec=2
				odpowiedz= answer[109]
				x=0
				y=0
				narracja = answer[110]
	
			elif rage<5 and calm>10:
				czykoniec=3
				odpowiedz=answer[111]
				x=0
				y=0
				narracja=answer[112]
	
			elif rage>=5 and calm>10:
				czykoniec=4
				odpowiedz=answer[113]
				x=0
				y=0
				narracja=answer[114]
		return odpowiedz,x,y,narracja,czykoniec,calka
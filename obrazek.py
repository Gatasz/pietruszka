def tabelka(ax):
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from matplotlib.table import Table
    import random
    a=random.randint(0,2)
    tb = Table(ax, bbox=[0,0,1,1])
    if a==0:
        for i in range(10):
            for j in range(10):
                tb.add_cell(i, j, 1, 1, loc='center',facecolor='w', edgecolor='k')
        rzad01=[2,4,6]
        for i in range(len(rzad01)):
            tb.add_cell(0, rzad01[i], 1, 1, loc='center', facecolor='k')
            tb.add_cell(1, rzad01[i], 1, 1, loc='center', facecolor='k')
        rzad3=[2,3,4,5,6,7]
        for i in range(len(rzad3)):
            tb.add_cell(3, rzad3[i], 1, 1, loc='center', facecolor='k')
        rzad4=[2,3,5,6,7,8,9]
        for i in range(len(rzad4)):
            tb.add_cell(4, rzad4[i], 1, 1, loc='center', facecolor='k')
        rzad5=[2,3,5,6,7,9]
        for i in range(len(rzad5)):
            tb.add_cell(5, rzad5[i], 1, 1, loc='center', facecolor='k')
        rzad6=[2,3,4,5,6,7,8]
        for i in range(len(rzad6)):
            tb.add_cell(6, rzad6[i], 1, 1, loc='center', facecolor='k')
        rzad7=[2,3,4,5,6,7]
        for i in range(len(rzad7)):
            tb.add_cell(7, rzad7[i], 1, 1, loc='center', facecolor='k')
        rzad8=[0,3,4,5,6,9]
        for i in range(len(rzad8)):
            tb.add_cell(8, rzad8[i], 1, 1, loc='center', facecolor='k')
        rzad9=[1,2,3,4,5,6,7,8]
        for i in range(len(rzad9)):
            tb.add_cell(9, rzad9[i], 1, 1, loc='center', facecolor='k')
    if a==1:
        for i in range(10):
            for j in range(10):
                tb.add_cell(i, j, 1, 1, loc='center',facecolor='w', edgecolor='k')
        rzad0=[6,7,8,9]
        for i in range(len(rzad0)):
            tb.add_cell(0, rzad0[i], 1, 1, loc='center', facecolor='k')
        rzad1=[3,4,5,9]
        for i in range(len(rzad1)):
            tb.add_cell(1,rzad1[i],1,1,loc='center', facecolor='k')
        rzad2=[3,7,8,9]
        for i in range(len(rzad2)):
            tb.add_cell(2,rzad2[i],1,1,loc='center', facecolor='k')
        rzad3=[3,4,5,6,9]
        for i in range(len(rzad3)):
            tb.add_cell(3, rzad3[i], 1, 1, loc='center', facecolor='k')
        rzad4=[3,9]
        for i in range(len(rzad4)):
            tb.add_cell(4, rzad4[i], 1, 1, loc='center', facecolor='k')
        rzad5=[3,7,8,9]
        for i in range(len(rzad5)):
            tb.add_cell(5, rzad5[i], 1, 1, loc='center', facecolor='k')
        rzad6=[1,2,3,6,7,8,9]
        for i in range(len(rzad6)):
            tb.add_cell(6, rzad6[i], 1, 1, loc='center', facecolor='k')
        rzad7=[0,1,2,3,6,7,8,9]
        for i in range(len(rzad7)):
            tb.add_cell(7, rzad7[i], 1, 1, loc='center', facecolor='k')
        rzad8=[0,1,2,3,7,8]
        for i in range(len(rzad8)):
            tb.add_cell(8, rzad8[i], 1, 1, loc='center', facecolor='k')
        rzad9=[1,2]
        for i in range(len(rzad9)):
            tb.add_cell(9, rzad9[i], 1, 1, loc='center', facecolor='k')
    if a==2:
        for i in range(10):
            for j in range(10):
                tb.add_cell(i, j, 1, 1, loc='center',facecolor='w', edgecolor='k')
        rzad0=[4,5,6,7,8,9]
        for i in range(len(rzad0)):
            tb.add_cell(0, rzad0[i], 1, 1, loc='center', facecolor='k')
        rzad1=[3,5,7,9]
        for i in range(len(rzad1)):
            tb.add_cell(1,rzad1[i],1,1,loc='center', facecolor='k')
        rzad2=[2,3,5,7,8]
        for i in range(len(rzad2)):
            tb.add_cell(2,rzad2[i],1,1,loc='center', facecolor='k')
        rzad3=[1,3,5,6,8]
        for i in range(len(rzad3)):
            tb.add_cell(3, rzad3[i], 1, 1, loc='center', facecolor='k')
        rzad4=[1,3,5,6,7,8]
        for i in range(len(rzad4)):
            tb.add_cell(4, rzad4[i], 1, 1, loc='center', facecolor='k')
        rzad5=[1,3,4,8]
        for i in range(len(rzad5)):
            tb.add_cell(5, rzad5[i], 1, 1, loc='center', facecolor='k')
        rzad6=[1,2,3,4,5,6,7,8]
        for i in range(len(rzad6)):
            tb.add_cell(6, rzad6[i], 1, 1, loc='center', facecolor='k')
        rzad7=[2,7]
        for i in range(len(rzad7)):
            tb.add_cell(7, rzad7[i], 1, 1, loc='center', facecolor='k')
        rzad8=[1,3,4,5,6]
        for i in range(len(rzad8)):
            tb.add_cell(8, rzad8[i], 1, 1, loc='center', facecolor='k')
        rzad9=[0,1]
        for i in range(len(rzad9)):
            tb.add_cell(9, rzad9[i], 1, 1, loc='center', facecolor='k')
    return tb